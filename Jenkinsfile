#!/usr/bin/env groovy

def config = [
    sonar: {

    }
]

node {
    stage('Checkout') {
        retry(3) { checkout scm }
        def pom = readMavenPom file: 'pom.xml'

        if (pom) {
            echo "Building branch ${env.BRANCH_NAME} at version ${ pom.version }"
        }
    }

    stage('Build') {
        withEnv(["PATH+jdk=${ tool 'JAVA 11' }/bin"]) {
            sh "./mvnw compile"
        }
    }

    stage('Unit tests') {
        withEnv(["PATH+jdk=${ tool 'JAVA 11' }/bin"]) {
            sh "./mvnw test"
            archiveArtifacts artifacts: '**/target/*.*'
        }
    }

    stage('Sonar analysis') {
        withEnv(["PATH+jdk=${ tool 'JAVA 11' }/bin"]) {
            sh "./mvnw verify org.sonarsource.scanner.maven:sonar-maven-plugin:sonar -Dsonar.token=${SONAR_TOKEN}"
            archiveArtifacts artifacts: '**/target/*.*'
        }
    }
}
